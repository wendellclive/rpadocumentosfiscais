# RPA Serviço de Automação
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Serviço em python para automação de seleção de documentos fiscais com redirecionamento para pastas de clientes.

### Requerimentos de Instalação
```sh
$ pip install -r requirements.txt
```

