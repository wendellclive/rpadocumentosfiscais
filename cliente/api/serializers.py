from rest_framework.serializers import ModelSerializer
from cliente.models import Cliente


class ClienteSerializer(ModelSerializer):
    class Meta:
        model = Cliente
        fields = ['razaoSocial', 'cnpj', 'pasta_destino']