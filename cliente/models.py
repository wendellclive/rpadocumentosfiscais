from django.db import models


class Cliente(models.Model):
    razaoSocial = models.CharField(max_length=150)
    cnpj = models.CharField(max_length=15)
    pasta_destino = models.CharField(max_length=25)

    def __str__(self):
        return self.razaoSocial
