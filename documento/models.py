from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from cliente.models import Cliente

class Documento(models.Model):
    nome = models.CharField(blank=False, max_length=250)
    sigla = models.CharField(blank=False, max_length=10)
    competencia = models.IntegerField(blank=False, validators=[MinValueValidator(1), MaxValueValidator(12)])
    destinatario = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    vencimento = models.DateField()

    def __str__(self):
        return self.nome