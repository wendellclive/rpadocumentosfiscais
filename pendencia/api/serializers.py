from rest_framework.serializers import ModelSerializer
from pendencia.models import Pendencia


class PendenciaSerializer(ModelSerializer):
    class Meta:
        model = Pendencia
        fields = ['id','arquivo', 'motivo']