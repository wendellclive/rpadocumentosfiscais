from rest_framework import viewsets
from pendencia.models import Pendencia

from .serializers import PendenciaSerializer


class PendenciaViewSet(viewsets.ModelViewSet):

    queryset = Pendencia.objects.all()
    serializer_class = PendenciaSerializer