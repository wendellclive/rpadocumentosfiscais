from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

class Pendencia(models.Model):
    arquivo = models.CharField(blank=False, max_length=250)
    motivo = models.TextField(blank=False)

    def __str__(self):
        return self.arquivo