#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  5 14:21:10 2020

@author: wendell
"""

#listar arqauivos do diretorio
import os
from os import listdir
import json, requests
import textract
import time

def tempo():
    time.sleep(10)    
    print ("Iniciou Processo de leitura...")

def busca_resolvido(arquivo=None):

    response = requests.get("http://localhost:8000/pendencia")
    conteudo = json.loads(response.content)

    for conta in conteudo:
        print(conta['arquivo'])
        print(conta['id'])
        if (arquivo == conta['arquivo']):
            response = requests.delete("http://localhost:8000/pendencia/"+str(conta['id']))
    return conteudo

       
def busca_compet(manipulador=None):
    
    comp_res = ''
    
    manipulador.seek(0)
    
    for linha_ in manipulador:
        linha_ = linha_.rstrip()
        
        if 'Competência' in linha_:

            comp_res = linha_[linha_.find('/')+1:]
            print("split de competencia", comp_res)  

    return comp_res

def busca_mes_compet(manipulador=None):

    mes_res = ''
    
    for linha in manipulador:
        linha = linha.rstrip()
        
        if 'Competência' in linha:

            mes_res = linha[-7:linha.find('/')]
            print("split de mes", mes_res)  

    return mes_res

def busca_extensao_arquivo(arquivo=None):
    
    extensao = arquivo[-3:]
    print("split de extensao", extensao)
    return extensao

def busca_cnpj(arquivo=None):
    
    cnpj_res = ''
    
    manipulador.seek(0)
    
    for linha_ in manipulador:
        linha_ = linha_.rstrip()
        
        if 'CNPJ' in linha_:

            cnpj_res = linha_[linha_.find('')+5:]
            print("split de cnpj", cnpj_res)  

    return cnpj_res

def busca_documento():

    response = requests.get("http://localhost:8000/documento")
    conteudo = json.loads(response.content)
    cont = 0
    for conta in conteudo:
        print(conteudo[cont]['nome'])
        print(conteudo[cont]['sigla'])
        cont = cont + 1
    return conteudo

def busca_cliente(cnpj=None):
    response = requests.get("http://localhost:8000/cliente")
    conteudo = json.loads(response.content)
    cont = 0

    cnpj = cnpj.replace('.', '')
    cnpj = cnpj.replace('/', '')
    cnpj = cnpj.replace('-','')

    cliente = ''
    
    for conta in conteudo:
        print ("cnpj      ", cnpj)
        print ("cnpj banco",conteudo[cont]['cnpj'])
        if (cnpj == conteudo[cont]['cnpj']):
            print(conteudo[cont]['razaoSocial'])
            print(conteudo[cont]['cnpj'])
            cliente = conteudo[cont]['razaoSocial']
        cont = cont + 1
    
    cliente = cliente.replace(' ', '')
    print("cliente", cliente)
    return cliente

def listar_arquivos(caminho=None):
    lista_arqs = [arq for arq in listdir(caminho)]
    return lista_arqs

def renomear_arquivo(caminho=None, arquivo=None, mes=None, competencia=None, sigla=None, extensao_arquivo=None, cliente=None):
    
    
    if (cliente != ''):
        
        novo_caminho = "/home/wendell/Área de Trabalho/" + cliente + "/impostos/" + sigla + "/"
        
        if os.path.exists(novo_caminho):
            print("O diretório " + novo_caminho + " existe.") 
            print("caminho recebido", caminho, " - arquivo recebido ", arquivo)
        else:
            os.makedirs(novo_caminho)
            print("O diretório " + novo_caminho + " NÃO existe.")
    
        renomeado = novo_caminho + "M" + competencia + "." + mes + "-" + sigla+"." + extensao_arquivo
        os.rename(caminho+arquivo, renomeado)
        return renomeado
    else:
        renomeado = "M" + competencia + "." + mes + "-" + sigla+"." + extensao_arquivo
        os.rename(caminho+arquivo, caminho+renomeado)
        motivo = ''
        
        if (sigla==''):        
            motivo = "Tipo de Documento Não cadastrado "
        
        if (cliente == ''):
            motivo = "Cliente Não cadastrado na Base " + motivo

        dados = data={"arquivo": renomeado, "motivo": motivo}
        
        response = requests.get("http://localhost:8000/pendencia")
        conteudo = json.loads(response.content)

        for linha in conteudo:
            
            print(conteudo)
            print(renomeado)                
            print(linha['arquivo'])
            
            if (linha['arquivo'] == renomeado):
                print(" RESULTADO", renomeado == linha['arquivo'])
                return renomeado
        
        print("gravei no banco")
        response = requests.post("http://localhost:8000/pendencia/", data=dados)
        print("RESPONSE :", response)

    return renomeado
    
if __name__ == '__main__':

    while(True):
        caminho="/home/wendell/Área de Trabalho/Desafio 3s/"
        arquivos = listar_arquivos(caminho)
        cont = 0;
    
        documentos = busca_documento() 
        
        for arquivo in arquivos:
            print("\nAberto: ", arquivo)
            cont = cont + 1;
            
            res = busca_extensao_arquivo(arquivo)
            print (res)
            
            if (res == 'pdf'):
                caminho_arquivo = caminho+arquivo
                manipulador = textract.process(caminho_arquivo)
                print("É PDF")
                end_temp = '/home/wendell/Área de Trabalho/abc/'
                temp = open(end_temp+arquivo, 'w')
                for linha in manipulador.decode('utf-8').split('\n'):
                    linha = linha.rstrip()
                    print (linha)
                    temp.write(linha);
                    temp.write('\n');
                    print(temp)
                temp.close()
                manipulador = open(end_temp+arquivo, 'r')
                #print(manipulador.decode('utf-8')[0:])
                #manipulador = manipulador.decode('utf-8')
            else:
                caminho_arquivo = caminho+arquivo
                manipulador = open(caminho_arquivo, 'r')
                print("NÃO É PDF")
                print(manipulador)
                
            contador = 0
                    
            for linha in manipulador:
    
                linha = str(linha).rstrip()
    
                for documento in documentos:
                    if documento['nome'] in linha:
                        if len(linha) > 0:                    
                            print ("Encontrada na linha", contador ," : ", linha)
                            mes = busca_mes_compet(manipulador)
                            competencia = busca_compet(manipulador)
                            extensao_arquivo = busca_extensao_arquivo(arquivo)
                            sigla = documento['sigla']
                            cnpj = busca_cnpj(manipulador)
                            cliente = busca_cliente(cnpj)
                            if (cliente != ''):
                                print ("Renomeando arquivo...", arquivo)
                                busca_resolvido(arquivo)
                            print ("Renomeado para :", renomear_arquivo(caminho, arquivo, mes, competencia, sigla, extensao_arquivo, cliente))
                            print ("Arquivo Fechado")
                            print ("\n")
                        else:
                            print ("Nada Encontrado")
                            print ("Arquivo Fechado")
                            print ("\n")
                contador = contador + 1
                
        manipulador.close()
        

        tempo()
        