#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 21:57:27 2020

@author: wendell
"""

# importing required modules
import PyPDF2

# creating a pdf file object
pdfFileObj = open("/home/wendell/Área de Trabalho/Desafio 3s/a.pdf", 'rb')

# creating a pdf reader object
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

# printing number of pages in pdf file
print("FOLHAS", pdfReader.numPages)

# creating a page object
pageObj = pdfReader.getPage(0)

# extracting text from page
print(pageObj.extractText())

# closing the pdf file object
pdfFileObj.close()